ago==0.0.93
alembic==1.0.7
amqpy==0.13.1
appdirs==1.4.3
argon2-cffi==19.1.0
astroid==2.0.4
atomicwrites==1.3.0
attrs==18.2.0
backcall==0.1.0
beautifulsoup4==4.7.1
black==18.9b0
bleach==3.1.0
certifi==2018.11.29
cffi==1.11.5
chardet==3.0.4
Click==7.0
cornice==3.5.1
decorator==4.3.2
dodgy==0.1.9
freezegun==0.3.11
gunicorn==19.9.0
html5lib==1.0.1
hupper==1.4.2
idna==2.8
ipython==7.2.0
ipython-genutils==0.2.0
isort==4.3.4
jedi==0.13.2
Jinja2==2.10
lazy-object-proxy==1.3.1
Mako==1.0.7
MarkupSafe==1.1.0
marshmallow==2.18.0
mccabe==0.6.1
more-itertools==5.0.0
mypy==0.660
mypy-extensions==0.4.1
parso==0.3.3
PasteDeploy==2.0.1
pep8-naming==0.4.1
pexpect==4.6.0
pickleshare==0.7.5
Pillow==5.4.1
plaster==1.0
plaster-pastedeploy==0.6
pluggy==0.8.1
prometheus-client==0.5.0
prompt-toolkit==2.0.8
prospector==1.1.6.2
psycopg2==2.7.7
ptyprocess==0.6.0
publicsuffix2==2.20160818
py==1.7.0
pycodestyle==2.4.0
pycparser==2.19
pydocstyle==3.0.0
pyflakes==1.6.0
Pygments==2.3.1
pylint==2.1.1
pylint-celery==0.3
pylint-django==2.0.2
pylint-flask==0.5
pylint-plugin-utils==0.4
pyotp==2.2.7
pyramid==1.10.2
pyramid-debugtoolbar==4.5
pyramid-ipython==0.2
pyramid-jinja2==2.8
pyramid-mako==1.0.2
pyramid-session-redis==1.4.1
pyramid-tm==2.2.1
pyramid-webassets==0.10
pytest==4.2.0
pytest-mock==1.10.1
python-dateutil==2.8.0
python-editor==1.0.4
PyYAML==3.13
qrcode==6.1
redis==3.1.0
repoze.lru==0.7
requests==2.21.0
requirements-detector==0.6
sentry-sdk==0.7.1
setoptconf==0.2.0
simplejson==3.16.0
six==1.12.0
snowballstemmer==1.2.1
soupsieve==1.7.3
SQLAlchemy==1.2.17
SQLAlchemy-Utils==0.33.11
stripe==2.20.3
testing.common.database==2.0.3
testing.redis==1.1.1
titlecase==0.12.0
toml==0.10.0
traitlets==4.3.2
transaction==2.4.0
translationstring==1.3
typed-ast==1.2.0
urllib3==1.24.1
venusian==1.2.0
waitress==1.2.1
wcwidth==0.1.7
webargs==4.4.1
webassets==0.12.1
webencodings==0.5.1
WebOb==1.8.5
WebTest==2.0.32
wrapt==1.11.1
zope.deprecation==4.4.0
zope.interface==4.6.0
zope.sqlalchemy==1.1
