ini_file: production.ini
ssl_cert_path: /etc/letsencrypt/live/tildes.net/fullchain.pem
ssl_private_key_path: /etc/letsencrypt/live/tildes.net/privkey.pem
hsts_max_age: 63072000
nginx_worker_processes: auto
postgresql_version: 10
prometheus_ips: ['127.0.0.1']
site_hostname: tildes.net
